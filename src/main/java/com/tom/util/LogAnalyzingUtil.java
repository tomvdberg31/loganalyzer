package com.tom.util;

import com.tom.domain.LogEntryType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The {@code LogAnalyzingUtil} class defines utility methods to read and analyze logfiles.
 *
 * @author Tomvdb
 * @since 29/09/2019
 */
public final class LogAnalyzingUtil {

    private static final String REQUEST_DOCUMENT_TEXT = "Executing request startRendering with arguments";
    private static final String PREPARING_DOCUMENT_TEXT = "Service startRendering returned";
    private static final String START_RENDERING_TEXT = "Executing request getRendering with arguments";
    private static final String END_RENDERING_TEXT = "Posting event: rendering##";

    private static final String DOC_ID_PATTERN = "(?<=\\[)\\d+(?=,)";
    private static final String THREAD_NUMBER_PATTERN = "(?<=\\[WorkerThread-)\\d+(?=\\])";
    private static final String TIMESTAMP_PATTERN = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2},\\d{3}";
    private static final String PAGE_PATTERN = "(?<=, )\\d+(?=\\])";
    private static final String UID_PATTERN = "\\d{7,15}+-\\d{1,6}+";

    private static final String DATE_FORMATTER = "yyyy-MM-dd HH:mm:ss,SSS";

    /**
     * Checks the given {@code logEntry} for its type and returns it.
     *
     * @param logEntry the log entry to check
     * @return the type of log entry
     */
    public static LogEntryType getEntryType(String logEntry) {
        LogEntryType type = null;
        if (logEntry.contains(REQUEST_DOCUMENT_TEXT)) {
            type = LogEntryType.REQUEST_DOCUMENT;
        } else if (logEntry.contains(PREPARING_DOCUMENT_TEXT)) {
            type = LogEntryType.PREPARING_DOCUMENT;
        } else if (logEntry.contains(START_RENDERING_TEXT)) {
            type = LogEntryType.START_RENDERING;
        } else if (logEntry.contains(END_RENDERING_TEXT)) {
            type = LogEntryType.END_RENDERING;
        }
        return type;
    }

    /**
     * Extracts and returns the document ID from the given {@code logEntry}.
     *
     * @param logEntry the log entry
     * @return the document ID
     */
    public static Long getDocumentID(String logEntry) {
        Long documentID = null;
        Matcher matcher = Pattern.compile(DOC_ID_PATTERN).matcher(logEntry);
        if (matcher.find()) {
            documentID = Long.parseLong(matcher.group());
        }
        return documentID;
    }

    /**
     * Extracts and returns the timestamp from the given {@code logEntry}.
     *
     * @param logEntry the log entry
     * @return timestamp
     */
    public static LocalDateTime getTimestamp(String logEntry) {
        LocalDateTime timestamp = null;
        Matcher matcher = Pattern.compile(TIMESTAMP_PATTERN).matcher(logEntry);
        if (matcher.find()) {
            timestamp = formatDate(matcher.group());
        }
        return timestamp;
    }

    /**
     * Extracts and returns the thread number from the given {@code logEntry}.
     *
     * @param logEntry the log entry
     * @return the thread number
     */
    public static Long getThreadNumber(String logEntry) {
        Long threadNumber = null;
        Matcher matcher = Pattern.compile(THREAD_NUMBER_PATTERN).matcher(logEntry);
        if (matcher.find()) {
            threadNumber = Long.parseLong(matcher.group());
        }
        return threadNumber;
    }

    /**
     * Extracts and returns the page number from the given {@code logEntry}.
     *
     * @param logEntry the log entry
     * @return the page number
     */
    public static Long getPageNumber(String logEntry) {
        Long pageNumber = null;
        Matcher matcher = Pattern.compile(PAGE_PATTERN).matcher(logEntry);
        if (matcher.find()) {
            pageNumber = Long.parseLong(matcher.group());
        }
        return pageNumber;
    }

    /**
     * Extracts and returns the uid from the given {@code logEntry}.
     *
     * @param logEntry the log entry
     * @return the uid
     */
    public static String getUID(String logEntry) {
        String uid = null;
        Matcher matcher = Pattern.compile(UID_PATTERN).matcher(logEntry);
        if (matcher.find()) {
            uid = matcher.group();
        }
        return uid;
    }

    /**
     * Converts the given {@code dateString} to a {@link LocalDateTime} instance.
     *
     * @param dateString the date string
     * @return the {@link LocalDateTime} instance
     */
    private static LocalDateTime formatDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        return LocalDateTime.parse(dateString, formatter);
    }

    /**
     * Private constructor to hide the default implicit one.
     */
    private LogAnalyzingUtil() {
    }
}
