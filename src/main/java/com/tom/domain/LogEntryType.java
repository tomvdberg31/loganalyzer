package com.tom.domain;

/**
 * The {@code LogEntryType} enum defines the different types of relevant log entries.
 *
 * @author Tomvdb
 * @since 25/09/2019
 */
public enum LogEntryType {

    REQUEST_DOCUMENT,
    PREPARING_DOCUMENT,
    START_RENDERING,
    END_RENDERING
}
