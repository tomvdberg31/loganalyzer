package com.tom.domain;

import lombok.Data;


/**
 * The {@code Summary} class is used to generate a summary of all analyzed {@Link Rendering} objects.
 *
 * @author Tom
 * @since 25/09/2019
 */
@Data
public class Summary {

    private int numberOfRenderings;
    private int numberOfDoubleRenderings; //meerdere starts met zelfde uid
    private int numberOfStartRenderingsWithoutGet;


    public void addDoubleRendering() {
        numberOfDoubleRenderings++;
    }

    public void addStartRenderingWithoutGet() {
        numberOfStartRenderingsWithoutGet++;
    }


}
