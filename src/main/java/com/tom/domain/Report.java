package com.tom.domain;

import lombok.Data;

import java.util.List;


// TODO: definitie van report klasse opmaken
/**
 * The {@code Report} class represents ...
 *
 * @author Tom
 * @since 28/09/2019
 */
@Data
public class Report {

    private List<Rendering> renderings;
    private Summary summary;


    public void printSummary() {
        System.out.println(summary.getNumberOfRenderings());
    }

    public void printRenderings() {
        for (Rendering rendering : renderings) {
            System.out.println(rendering.toString());
        }
    }
}
