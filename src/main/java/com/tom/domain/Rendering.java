package com.tom.domain;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Rendering} class represents a document which has been requested and rendered one or more times.
 *
 * @author Tom
 * @since 25/09/2019
 */
@Data
public class Rendering {

    private String uid;
    private Long documentID;
    private Long page;
    private Long threadNumber;
    // question: lombok creeert set/get voor deze twee lijsten, maar die zijn te verwarren met onderstaande 2 methoden +
    // misschien lijsten final maken en ze in de constructor instantieren
    private List<LocalDateTime> startRenderingTimestamps;
    private List<LocalDateTime> endRenderingTimestamps;

    public Rendering() {
        startRenderingTimestamps = new ArrayList<>();
        endRenderingTimestamps = new ArrayList<>();
    }

    /**
     * Adds a {@link LocalDateTime} timestamp to the {@code startRenderingTimestamps} list
     *
     * @param timestamp the timestamp to be added
     */
    public void addStartRenderingTimestamp(LocalDateTime timestamp) {
        startRenderingTimestamps.add(timestamp);
    }


    /**
     * Adds a {@link LocalDateTime} timestamp to the {@code endRenderingTimestamps} list
     *
     * @param timestamp the timestamp to be added
     */
    public void addEndRenderingTimestamp(LocalDateTime timestamp) {
        endRenderingTimestamps.add(timestamp);
    }


    @Override
    public String toString() {
        return "Rendering{" +
                "uid='" + uid + '\'' +
                ", documentID=" + documentID +
                ", page=" + page +
                ", threadNumber=" + threadNumber +
                '}';
    }
}
