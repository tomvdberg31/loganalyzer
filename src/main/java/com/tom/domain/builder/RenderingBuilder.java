package com.tom.domain.builder;

import com.google.common.collect.Lists;
import com.tom.domain.Rendering;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Rendering} class is user to create new instances of the {@link RenderingBuilder} class.
 *
 * @author Tomvdb
 * @since 26/09/2019
 */
public final class RenderingBuilder {

    private final Long documentID;
    private final Long page;
    private final Long threadNumber;

    private String uid;
    private List<LocalDateTime> start;
    private List<LocalDateTime> end;

    /**
     * Creates a new instance of the {@link RenderingBuilder} class with the given {@code documentID}, {@code page},
     * and {@code threadNumber}.
     *
     * @param documentID   the document ID
     * @param page         the page number
     * @param threadNumber the thread number
     */
    private RenderingBuilder(Long documentID, Long page, Long threadNumber) {
        this.documentID = documentID;
        this.page = page;
        this.threadNumber = threadNumber;

        start = new ArrayList<>();
        end = new ArrayList<>();
    }

    /**
     * The entry point to create a new {@link RenderingBuilder} instance.
     * <p>
     * Creates and returns a new {@link RenderingBuilder} class with the given {@code documentID}, {@code page},
     * and {@code threadNumber}.
     *
     * @param documentID   the document ID
     * @param page         the page number
     * @param threadNumber the thread number
     * @return the {@link RenderingBuilder} for method chaining
     */
    public static RenderingBuilder aRendering(Long documentID, Long page, Long threadNumber) {
        return new RenderingBuilder(documentID, page, threadNumber);
    }

    /**
     * Sets the given {@code uid} on the current {@link RenderingBuilder} instance.
     *
     * @param uid the rendering's unique identifier
     * @return the {@link RenderingBuilder} for method chaining
     */
    public RenderingBuilder withUID(String uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Adds the given {@code startTimes} to the current {@link RenderingBuilder} instance.
     *
     * @param startTimes the start times of the rendering
     * @return the {@link RenderingBuilder} for method chaining
     */
    public RenderingBuilder withStartTimes(LocalDateTime... startTimes) {
        start.addAll(Lists.newArrayList(startTimes));
        return this;
    }

    /**
     * Adds the given {@code startTimes} to the current {@link RenderingBuilder} instance.
     *
     * @param endTimes the end times of the rendering
     * @return the {@link RenderingBuilder} for method chaining
     */
    public RenderingBuilder withEndTimes(LocalDateTime... endTimes) {
        end.addAll(Lists.newArrayList(endTimes));
        return this;
    }

    /**
     * Creates and returns a new instance of the {@link Rendering} class.
     *
     * @return the {@link Rendering} instance
     */
    public Rendering build() {
        Rendering rendering = new Rendering();
        rendering.setUid(uid);
        rendering.setDocumentID(documentID);
        rendering.setPage(page);
        rendering.setThreadNumber(threadNumber);
        rendering.setStartRenderingTimestamps(start);
        rendering.setEndRenderingTimestamps(end);
        return rendering;
    }
}
