package com.tom.service;

import com.tom.domain.Report;
import com.tom.domain.Summary;

import java.io.File;

/**
 * This {@code LogAnalyzingService} interface defines the business logic to read and analyze logfiles.
 *
 * @author Tomvdb
 * @since 25/9/2019
 */
public interface LogAnalyzingService {

    /**
     * Analyzes the given {@code file}. Creates and returns a report.
     *
     * @param file The logfile
     * @return The report of the analysis
     */
    Report analyze(File file);

    /**
     * Creates and returns a {@link Summary} instance for the given {@code report}.
     *
     * @param report the report
     * @return the {@link Summary} instance
     */
    Summary createSummary(Report report);
}
