package com.tom.service.impl;

import com.tom.domain.LogEntryType;
import com.tom.domain.Rendering;
import com.tom.domain.Report;
import com.tom.domain.Summary;
import com.tom.domain.builder.RenderingBuilder;
import com.tom.service.LogAnalyzingService;
import com.tom.util.LogAnalyzingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code LogAnalyzingServiceImpl} class implements the business logic
 * defined in the {@link LogAnalyzingService} interface.
 *
 * @author Tomvdb
 * @since 25/09/2019
 */
public class LogAnalyzingServiceImpl implements LogAnalyzingService {

    private static final Logger LOG = LoggerFactory.getLogger(LogAnalyzingServiceImpl.class);

    // TODO: MAAK CONFIG FILE AAN OM BEAN THE WIREN
    // TODO: MAAK Application klasse aan

    /**
     * {@inheritDoc}
     */
    @Override
    public Report analyze(File file) {
        Report report = new Report();

        BufferedReader reader = getReader(file);
        if (reader != null) {
            String logEntry;
            try {
                List<Rendering> renderingList = new ArrayList<>();
                while ((logEntry = reader.readLine()) != null) {
                    LogEntryType type = LogAnalyzingUtil.getEntryType(logEntry);
                    if (type == null) {
                        // TODO: OOK ZEKER TESTEN
                        continue;
                    }

                    switch (type) {
                        case REQUEST_DOCUMENT:
                            findAndUpdateTimestampOrCreateRendering(logEntry, renderingList);
                            break;
                        case PREPARING_DOCUMENT:
                            findAndUpdateRenderingWithTheUID(logEntry, renderingList);
                            break;
                        case START_RENDERING:
                            updateWithEndRenderingTimestamp(logEntry, renderingList);
                            break;
                        case END_RENDERING:
                            // question waarschijnlijk is deze case overbodig
                            break;
                        default: // do nothing
                    }
                }
                report.setRenderings(renderingList);
            } catch (IOException e) {
                LOG.error(MessageFormat.format("An exception occurred while reading log entries: {0}.", e.getMessage()));
            }
        }

        // TODO: CREATE SUMMARY HERE BY LOOPING OVER THE RENDERINGS (NULL-SAFE) (Gebruik builders)

        report.setSummary(createSummary(report));

        return report;
    }

    /**
     * Finds a {@link Rendering} object with a uid from the {@code logEntry}. If it exists, the timestamp is added.
     *
     * @param logEntry the log entry
     * @param renderingList the rendering list
     */
    // todo test
    void updateWithEndRenderingTimestamp(String logEntry, List<Rendering> renderingList) {
        String uid = LogAnalyzingUtil.getUID(logEntry);
        LocalDateTime timestamp = LogAnalyzingUtil.getTimestamp(logEntry);

        for (Rendering rendering : renderingList) {
            if (uid.equals(rendering.getUid())) {
                rendering.addEndRenderingTimestamp(timestamp);
            }
        }
    }

    /**
     * Checks if a {@link Rendering} already exists for the extracted timestamp.
     * <p>
     * If not, a new {@link Rendering} object is created. Else the timestamp is added to the existing Rendering
     *
     * @param logEntry the log entry from which timestamp is extracted
     * @param renderingList the rendering list
     */
    // todo test
    void findAndUpdateTimestampOrCreateRendering(String logEntry, List<Rendering> renderingList) {
        Rendering rendering = getRenderingForDocumentIDAndThreadnumber(logEntry, renderingList);
        // question: ok om hier op NULL te checken?
        if (rendering == null) {
            createNewRendering(logEntry, renderingList);
        } else {
            rendering.addStartRenderingTimestamp(LogAnalyzingUtil.getTimestamp(logEntry));
        }
    }

    /**
     * Finds a {@Code Rendering} object with a documentID.
     *
     * @param logEntry the log entry
     * @param renderingList the list of renderings
     * @return a rendering or null
     */
    //todo test
    private Rendering getRenderingForDocumentIDAndThreadnumber(String logEntry, List<Rendering> renderingList) {
        Rendering rendering = null;
        Long documentID = LogAnalyzingUtil.getDocumentID(logEntry);
        Long threadNumber = LogAnalyzingUtil.getThreadNumber(logEntry);

        for (Rendering rendering1 : renderingList) {
            if (rendering1.getDocumentID().equals(documentID) && rendering1.getThreadNumber().equals(threadNumber)) {
                rendering = rendering1;
                break;
            }
        }
        return rendering;
    }

    /**
     * Creates a new {@link Rendering} from the given {@code logEntry} and adds it to the given {@code renderingList}.
     *
     * @param logEntry      the log entry
     * @param renderingList the renderings
     */
    private void createNewRendering(String logEntry, List<Rendering> renderingList) {
        renderingList.add(createRendering(logEntry));
    }

    /**
     * Find the the rendering based on the thread number from the given {@code renderingList}, and updates it with
     * the UID.
     * <p>
     * Both the thread number and UID are extracted from the given {@code logEntry}.
     *
     * @param logEntry      the log entry
     * @param renderingList the rendering list
     */
    private void findAndUpdateRenderingWithTheUID(String logEntry, List<Rendering> renderingList) {
        Long threadNumber = LogAnalyzingUtil.getThreadNumber(logEntry);
        Rendering rendering = getRenderingForThreadNumber(renderingList, threadNumber);
        if (rendering != null) {
            rendering.setUid(LogAnalyzingUtil.getUID(logEntry));
        }
    }

    /**
     * Checks whether there is a rendering available in the given {@code renderingList} which has not yet a uid assigned.
     * <p>
     * The check is done using the given {@code threadNumber}.
     *
     * @param renderingList the renderings
     * @param threadNumber  the thread number
     * @return the rendering
     */
    private Rendering getRenderingForThreadNumber(List<Rendering> renderingList, Long threadNumber) {
        Rendering result = null;

        for (Rendering rendering : renderingList) {
            if (rendering.getThreadNumber().equals(threadNumber) && rendering.getUid() == null) {
                result = rendering;
                break;
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Summary createSummary(Report report) {
        // TODO: IMPLEMENT + TEST
        Summary summary = new Summary();
        summary.setNumberOfRenderings(report.getRenderings().size());

        return summary;
    }

    /**
     * Returns a {@link BufferedReader} for the given {@code file}.
     *
     * @param file the file to read
     * @return the {@link BufferedReader} instance
     */
    BufferedReader getReader(File file) {
        BufferedReader reader = null;
        try {
            // TODO: MAAK EEN AFZONDERLIJKE TEST --> SPY in analyze
            // TODO: GEBRUIK POWERMOCK WHEN NEW
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            LOG.error(MessageFormat.format("An exception occurred while analyzing the log file: {0}.", e.getMessage()));
        }
        return reader;
    }


    /**
     * Extracts the document ID, page and thread number from the given {@code logEntry}. Afterwards a new
     * {@link Rendering} instance is created and returned.
     *
     * @param logEntry the log entry
     * @return the {@link Rendering} instance
     */
    private Rendering createRendering(String logEntry) {
        Long documentID = LogAnalyzingUtil.getDocumentID(logEntry);
        Long page = LogAnalyzingUtil.getPageNumber(logEntry);
        Long threadNumber = LogAnalyzingUtil.getThreadNumber(logEntry);

        return RenderingBuilder
                .aRendering(documentID, page, threadNumber)
                .build();
    }


}
