package com.tom;

import com.tom.domain.Report;
import com.tom.service.LogAnalyzingService;
import com.tom.service.impl.LogAnalyzingServiceImpl;

import java.io.File;

public class Application {


    public static void main(String[] args) {
        // TODO WRITE REPORT TO report.xml

        LogAnalyzingService logAnalyzingService = new LogAnalyzingServiceImpl();
        Report report = logAnalyzingService.analyze(new File("src/main/resources/server.log"));
        report.printSummary();
        report.printRenderings();
    }
}
