package com.tom.util;

import com.tom.domain.LogEntryType;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDateTime;

/**
 * The {@code LogAnalyzingUtilTest} class contains the unit tests for the {@link LogAnalyzingUtil} class.
 *
 * @author Tomvdb
 * @since 25/09/2019
 */
public class LogAnalyzingUtilTest {

    private static final String REQUEST_DOCUMENT_LOG_ENTRY = "2010-10-06 09:03:05,869 [WorkerThread-17] INFO  [ServiceProvider]: Executing request startRendering with arguments [114466, 0] on service object { ReflectionServiceObject -> com.dn.gaverzicht.dms.services.DocumentService@4a3a4a3a }";
    private static final String PREPARING_DOCUMENT_LOG_ENTRY = "2010-10-06 09:03:05,873 [WorkerThread-17] INFO  [ServiceProvider]: Service startRendering returned 1286373785873-3536";
    private static final String START_RENDERING_LOG_ENTRY = "2010-10-06 09:03:06,547 [WorkerThread-15] INFO  [ServiceProvider]: Executing request getRendering with arguments [1286373785873-3536] on service object { ReflectionServiceObject -> com.dn.gaverzicht.dms.services.DocumentService@4a3a4a3a }";
    private static final String END_RENDERING_LOG_ENTRY = "2010-10-06 09:03:59,469 [WorkerThread-6] INFO  [ServerSession]: Posting event: rendering##1286373837895-7889##finished";

    @Test
    public void testGetEntryTypeForRequestDocument() {
//        String logEntry = MessageFormat.format("{0}{1}{2}",
//                RandomStringUtils.random(10),
//                LogAnalyzingUtil.REQUEST_DOCUMENT_TEXT,
//                RandomStringUtils.random(10));

        LogEntryType result = LogAnalyzingUtil.getEntryType(REQUEST_DOCUMENT_LOG_ENTRY);

        Assertions
                .assertThat(result)
                .isNotNull()
                .isEqualTo(LogEntryType.REQUEST_DOCUMENT);
    }

    @Test
    public void testGetEntryTypeForPreparingDocument() {
        LogEntryType result = LogAnalyzingUtil.getEntryType(PREPARING_DOCUMENT_LOG_ENTRY);

        Assertions
                .assertThat(result)
                .isNotNull()
                .isEqualTo(LogEntryType.PREPARING_DOCUMENT);
    }

    @Test
    public void testGetEntryTypeForStartRendering() {
        LogEntryType result = LogAnalyzingUtil.getEntryType(START_RENDERING_LOG_ENTRY);

        Assertions
                .assertThat(result)
                .isNotNull()
                .isEqualTo(LogEntryType.START_RENDERING);
    }

    @Test
    public void testGetEntryTypeForEndRendering() {
        LogEntryType result = LogAnalyzingUtil.getEntryType(END_RENDERING_LOG_ENTRY);

        Assertions
                .assertThat(result)
                .isNotNull()
                .isEqualTo(LogEntryType.END_RENDERING);
    }

    @Test
    public void testGetEntryTypeForUnknownType() {
        String logEntry = RandomStringUtils.random(10);

        LogEntryType result = LogAnalyzingUtil.getEntryType(logEntry);

        Assertions
                .assertThat(result)
                .isNull();
    }

    @Test
    public void testGetDocumentIDWithMatch() {
        Long result = LogAnalyzingUtil.getDocumentID(REQUEST_DOCUMENT_LOG_ENTRY);

        Assertions
                .assertThat(result)
                .isEqualTo(114466);
    }

    @Test
    public void testGetDocumentIDWithoutMatch() {
        Long result = LogAnalyzingUtil.getDocumentID(RandomStringUtils.random(10));

        Assertions
                .assertThat(result)
                .isNull();
    }

    @Test
    public void testGetTimeStampWithMatch() {
        LocalDateTime result = LogAnalyzingUtil.getTimestamp(REQUEST_DOCUMENT_LOG_ENTRY);

        Assertions
                .assertThat(result.getNano())
                .isEqualTo(869000000);

        Assertions
                .assertThat(result.getSecond())
                .isEqualTo(5);

        Assertions
                .assertThat(result.getMinute())
                .isEqualTo(3);

        Assertions
                .assertThat(result.getHour())
                .isEqualTo(9);

        Assertions
                .assertThat(result.getDayOfMonth())
                .isEqualTo(6);

        Assertions
                .assertThat(result.getMonthValue())
                .isEqualTo(10);

        Assertions
                .assertThat(result.getYear())
                .isEqualTo(2010);
    }

    @Test
    public void testGetTimestampWithoutMatch() {
        LocalDateTime result = LogAnalyzingUtil.getTimestamp(RandomStringUtils.randomNumeric(10));

        Assertions
                .assertThat(result)
                .isNull();
    }

    @Test
    public void testGetThreadNumberWithMatch() {
        Long result = LogAnalyzingUtil.getThreadNumber(PREPARING_DOCUMENT_LOG_ENTRY);

        Assertions
                .assertThat(result)
                .isEqualTo(17);
    }

    @Test
    public void testGetThreadNumberWithoutMatch() {
        Long result = LogAnalyzingUtil.getThreadNumber(RandomStringUtils.randomNumeric(10));

        Assertions
                .assertThat(result)
                .isNull();
    }

    @Test
    public void testGetPageWithMatch() {
        Long result = LogAnalyzingUtil.getThreadNumber(REQUEST_DOCUMENT_LOG_ENTRY);

        Assertions
                .assertThat(result)
                .isEqualTo(17);
    }

    @Test
    public void testPageWithoutMatch() {
        Long result = LogAnalyzingUtil.getThreadNumber(RandomStringUtils.randomNumeric(10));

        Assertions
                .assertThat(result)
                .isNull();
    }

    @Test
    public void testGetUIDWithMatch() {
        String result = LogAnalyzingUtil.getUID(PREPARING_DOCUMENT_LOG_ENTRY);

        Assertions
                .assertThat(result)
                .isEqualTo("1286373785873-3536");
    }

    @Test
    public void testGetUIDWithoutMatch() {
        String result = LogAnalyzingUtil.getUID(RandomStringUtils.random(10));

        Assertions
                .assertThat(result)
                .isNull();
    }


}