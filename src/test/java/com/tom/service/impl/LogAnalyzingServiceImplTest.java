package com.tom.service.impl;

import com.tom.domain.Rendering;
import com.tom.domain.Report;
import com.tom.service.LogAnalyzingService;
import com.tom.util.LogAnalyzingUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * The {@Code LogAnalyzingServiceImplTest} class contains the unit tests for the {@link LogAnalyzingServiceImpl} class.
 *
 * @author Tomvdb
 * @since 28/09/2019
 */
// TODO: GEBRUIK POWERMOCK OM STATISCHE METHODES TE TESTEN
@RunWith(MockitoJUnitRunner.class)
public class LogAnalyzingServiceImplTest {

    //private static final String LOG_ENTRY = RandomStringUtils.random(10);
    private static final String UID = RandomStringUtils.random(10);
    private static final Long DOCUMENT_ID = RandomUtils.nextLong();
    private static final Long PAGE = RandomUtils.nextLong();
    private static final Long THREAD_NUMBER = RandomUtils.nextLong();

    private static final String LOG_ENTRY = "2010-10-06 09:03:06,547 [WorkerThread-15] INFO  " +
            "[ServiceProvider]: Executing request getRendering with " +
            "arguments [1286373785873-3536] on service object " +
            "{ ReflectionServiceObject -> " +
            "com.dn.gaverzicht.dms.services.DocumentService@4a3a4a3a }";

    private List<Rendering> renderingList;
    private Rendering rendering;


    @BeforeAll
    private void init() {
        rendering = new Rendering();
        rendering.setUid("1286373785873-3536");
        rendering.setDocumentID(DOCUMENT_ID);
        rendering.setPage(PAGE);
        rendering.setThreadNumber(THREAD_NUMBER);

        renderingList = new ArrayList<>();
        renderingList.add(rendering);

    }

    @Spy
    private LogAnalyzingServiceImpl logAnalyzingService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    // TODO: TEST EXCEPTIONS MET @RULE

    @Test
    public void testAnalyzeForRequestDocumentLogEntry() {
        File file = mock(File.class);

//        TODO: WHEN NEW GEBRUIKER VOOR READER, AANGEZIEN JE DE LOG_ENTRY WILT TERUGGEVEN

        when(LogAnalyzingUtil.getDocumentID(LOG_ENTRY)).thenReturn(DOCUMENT_ID);

        doNothing().when(logAnalyzingService.createSummary(any(Report.class)));

        Report result = logAnalyzingService.analyze(file);

        Assertions
                .assertThat(result.getRenderings())
                .isNotEmpty()
                .hasSize(1);

        // TODO: CUSTOM ASSERTION (optioneel)

        Rendering render = result.getRenderings().get(0);

        Assertions
                .assertThat(render)
                .isNotNull();

        Assertions
                .assertThat(render.getDocumentID())
                .isEqualTo(DOCUMENT_ID);
    }

    @Test
    public void testUpdateWithEndRenderingTimestamp() {

        String logEntry = "2010-10-06 09:03:06,547 [WorkerThread-15] INFO  " +
                "[ServiceProvider]: Executing request getRendering with " +
                "arguments [1286373785873-3536] on service object " +
                "{ ReflectionServiceObject -> " +
                "com.dn.gaverzicht.dms.services.DocumentService@4a3a4a3a }";

        LocalDateTime timestamp = LogAnalyzingUtil.getTimestamp(logEntry);
        String tempUID = LogAnalyzingUtil.getUID(logEntry);

        List<Rendering> renderingList = new ArrayList<>();
        Rendering rendering = new Rendering();

        rendering.setUid(tempUID);
        renderingList.add(rendering);

        logAnalyzingService.updateWithEndRenderingTimestamp(logEntry, renderingList);

        LocalDateTime result =renderingList.get(0).getEndRenderingTimestamps().get(0);

        Assertions.assertThat(result.toString()).isEqualTo(timestamp.toString());
    }

    @Test
    public void testFailUpdateWithEndRenderingTimestamp() {
        String logEntry = "2010-10-06 09:03:06,547 [WorkerThread-15] INFO  " +
                "[ServiceProvider]: Executing request getRendering with " +
                "arguments [1286373785873-3536] on service object " +
                "{ ReflectionServiceObject -> " +
                "com.dn.gaverzicht.dms.services.DocumentService@4a3a4a3a }";

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");
        LocalDateTime otherTimestamp = LocalDateTime.parse("2015-01-01 08:10:23,317", dateFormatter);

        List<Rendering> renderingList = new ArrayList<>();
        Rendering rendering = new Rendering();

        rendering.setUid(RandomStringUtils.random(10));
        renderingList.add(rendering);

        logAnalyzingService.updateWithEndRenderingTimestamp(logEntry, renderingList);

        List result =renderingList.get(0).getEndRenderingTimestamps();

        Assertions
                .assertThat(result)
                .isEmpty();
    }

    @Test
    public void testFindAndUpdateTimestampOrCreateRendering () {


    }

    @Test
    public void testGetRenderingForDocumentIDAndTimestamp() {

    }
}